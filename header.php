
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />

		<title><?php wp_title( 'Surfers Paradise', true, 'right' ); ?></title>
		
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
		<link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace&display=swap" rel="stylesheet"> 
		<link href="https://fonts.googleapis.com/css?family=Darker+Grotesque&display=swap" rel="stylesheet"> 
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/font-awesome.css">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<script type="text/javascript" src="<?php  echo esc_url( get_template_directory_uri() ); ?>/js/jquery-3.4.1.min.js">	
		</script>
		<script type="text/javascript" src="<?php  echo esc_url( get_template_directory_uri() ); ?>/js/efectos.js">	
		</script>
		<header>				
			<div class="row contenedor_logo_menu">
				<div class="logo col-xs-12 col-md-6">
					<a href="#"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.jpg" width="200" alt=""></a>
				</div>
			
				<div class="menu col-xs-12 col-md-6">
					<a href="#" id="btn-home">	Home</a>
					<a href="#" id="btn-whatwedo">What we do</a>
					<a href="#" id="btn-whoweare">Who we are</a>
					<a href="#" id="btn-lessons" >Our lessons</a>
					<a href="#" id="btn-contact" >Contact us</a>
					<a href="https://surfersparadise.bookinglayer.io/frontoffice/#/" target="_blank">-Book-</a>
				</div>
			</div>
					
			<div class="contenedor" id="home">
				<div class="contenedor-texto">
					<div class="texto">
						<h1 class="nombre"><b>Surfers Paradise</b></h1>
						<h2 class="profesion"><b>SRI LANKA</b></h2>
						<p class="texto1"><b>"A heaven for beginner and intermediate surfers in beautiful Weligama, Sri Lanka. Best rated surf school in Weligama". </b> </p>
					</div>	
				</div>
			</div>		
		</header>