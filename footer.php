<footer>
	
	<div class="contenedor" id="contact">
		<div class="titulo">
			<h2 class="contact">-Contact us-</h2>	
		</div>
		
		<div class="datos">
			<div class="pic">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/contact.jpg" width="200" alt="">	
			</div>

			<div class="info">
				<p class="parrafo">
					<b>Phone Number / <i class="fa fa-whatsapp"></i></b> = +94 7676 88988 <br>
						<b>Email:	</b> lankasurfersparadise@gmail.com <br>
						<b>Directions:	</b> 
						<br> <i class="fa fa-plane"></i> To get to Weligama from the airport we recommend using: Uber, you can also use the train along the coast, as well as buses.
						We are located on Weligama’s main beach across the restaurant “chill bay” <br>
						<i class="fa fa-hand-o-right"></i> <b>Our address is:	</b> "Surfers Paradise", Weligama Bay, Weligama, Srilanka</p>	
			</div>	
		</div>
				
		
	</div>



	<section class="redes-sociales">
			<div class="contenedor">
			<a class="facebook" href="https://m.facebook.com/surfersparadiseweligama/" target="_blank"><i class="fa fa-facebook"></i></a>
			<a class="google" href="https://www.google.com/maps/place/Surfers+Paradise+Sri+Lanka/@5.9728003,80.4353192,15z/data=!4m5!3m4!1s0x0:0xabd3337f80a97141!8m2!3d5.9728003!4d80.4353192" target="_blank"><i class="fa fa-google"></i></a>
			<a class="trip" href="https://www.tripadvisor.co.uk/Attraction_Review-g612380-d14023844-Reviews-Surfers_Paradise-Weligama_Matara_Southern_Province.html"><i class="fa fa-tripadvisor" target="_blank"></i></a>
			<a class="instagram" href="https://www.instagram.com/surfers_paradise_sri_lanka/" target="_blank"><i class="fa fa-instagram"></i></a>
			</div>
	</section>
				<div class="contenedor">
			<div class="copyright">
				<a href="https://www.upwork.com/freelancers/~017b6e58d6cd37bcad" target="_blank" >"Designed by Sophia Funes - All rights reserved © 2019</a>
			</div>
		</div>



	
</footer>



	<?php wp_footer(); //Crucial footer hook! ?>
</body>
</html>
