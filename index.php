<?php get_header(); ?>


<body>

	
		

	<section class="main">
	<section class="acerca" id="whatwedo"><div class="contenedor"><div class="whatwedo" >
			
				<h2 class="titulo">-What we do-</h2>
				<p class="parrafo">
					<span class="bold">Our goal is to make you enjoy the beautiful Weligama beach</span>, one of the best spots in the world to learn how to surf; beginner and intermediate surfer’s heaven, Weligama has a long sandy beach without reef, long waves (green and white), no current and warm water, this location makes Weligama accessible with great conditions, year round.
 				will provide you with everything you need to enjoy your time with us, with surfboard rentals (various type and size of boards) and body boards, as well as top quality surf lessons. We also have sunbeds, great atmosphere and chill music. We do offer surf camp packages as well, so you can really focus on your surfing skills and stay with us for an extended amount of time. <br>
 			<span class="bold">Our team of experienced instructors will make you a real surfer in no time</span>, always keeping your safety as a top priority; we will give you all the knowledge and tools needed to become an autonomous surfer. <span class="bold">We follow a teaching system that has proved itself successfully over the years</span>, and we always make sure you have fun while learning.</p 
 		</div></div>
	<div class="contenedor">
		
 	</div>
		</section>

	<section class="galeria">
		<div class="contenedor">
			<div class="fotowall">

				<div class="thumb">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/1.jpg" alt="descripcion de la imagen"> </div>
				<div class="thumb">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/2.jpg" alt="descripcion de la imagen"> </div>
   				<div class="thumb">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/3.jpg" alt="descripcion de la imagen"> </div>
   				<div class="thumb">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/4.jpg" alt="descripcion de la imagen"> </div>
   										
   				<div class="thumb">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/5.jpg" alt="descripcion de la imagen"> </div>
   				<div class="thumb">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/2019.jpg" alt="descripcion de la imagen"> </div>
   										
  </div>
</div>
		
	</section>
 		
			
	

			<section class="team">

				<div class="contenedor" >
			
				<div class="texto">
					<h3 class="titulo" id="whoweare">-Who we are-</h3>
				
				</div>

				<div class="contenedor-trabajos">

					<div class="trabajo">
						<div class="thumb">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/team/1.jpg" width="300" alt="Pragee - Founder and Instructor">
						</div>
						<div class="descripcion">
							<p class="nombre"><b>Pragee </b>- Founder and Instructor</p>
							<p class="categoria">Pragee has been working as a surf instructor in Weligama since 2011 and created Surfers Paradise. <br>He was born and raised in Weligama and can give you all the good tips on the area. 
Very welcoming and always smiling, he has a ton of experience teaching beginners and intermediates surfers, always putting the safety of his surfers first.</p>
						</div>
					</div>

					<div class="trabajo">
						<div class="thumb">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/team/2.jpg" width="300"  alt="Laure - Client services and Bookings">
						</div>
						<div class="descripcion">
							<p class="nombre"><b>Laure </b>- Client services and Bookings</p>
							<p class="categoria">Laure is originally from France, she then studied in the US and travelled the world. <br> With a background in finance and business, she now welcomes the clients and manages the internal operations. She fell in love with Sri Lanka while traveling and enjoys surfing at sunset.</p>
						</div>
					</div>

					<div class="trabajo">
						<div class="thumb">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/team/3.jpg" width="300" alt="Luna - Happiness Manager">
						</div>
						<div class="descripcion">
							<p class="nombre"><b>Luna </b>- Happiness Manager <i class="fa fa-heart"></i></p>
							<p class="categoria">Luna literally owns the beach… She loves rice and curry, especially if there is fish in it! She always keeps an eye on everyone’s belongings and enjoys the chill music of our school.
							</p>
						</div>
					</div>

					<h2 class="voluntario" id="voluntario">Want to volunteer with us? Please get in touch and send us an email.</h2>
	
			</div>
			</div>
		</section>

		<section class="lessons">
			<div class="contenedor" id="ourlessons">
				<div class="ourlessons"><h2 class="leccion">-Our Lessons-</h2>	</div>
				<div class="caja1"><div class="box">
					<div class="photo">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/c.jpg" width="300" alt=""></div>
					<div class="clases"><h2 class="tit">For Begginers</h2>
					<p class="textlesson"> <i class="fa fa-check" ></i> Our surf lessons are 75 minutes long. <br>	<i class="fa fa-clock-o" ></i>Approximately 15  – 20 minutes is spent on the beach covering safety and the basics. <br>	The remaining time is spent in the water, using a soft board we will help you to catch your first waves standing up like a pro, by always giving you feedback on your position and <technique class="br">	</technique> If you can’t get enough and you want to practice by yourself after the lesson, you can rent a surfboard and to keep working on it. <br>	<i class="fa fa-users"></i> We have one instructor for a maximum of three students to ensure a better progression.</p>	</div>
					
				</div>

				<div class="box">
					<div class="photo">	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/20.jpg" width="300" alt=""></div>
					<div class="clases"><h2 class="tit">For intermediate surfers / lineup lessons </h2>
					<p class="textlesson"><i class="fa fa-check" ></i>For people who can already catch green waves and looking to improve their skills and techniques. <br>	 <i class="fa fa-clock-o" ></i> The lessons are about 75 minutes long, you will catch green waves on a hardboard and work on different aspects of your technique. <br><i class="fa fa-users" ></i> Your instructor will give you detailed feedback and pointers to increase your skill set. </p>	</div>	</div>
				
					
				</div>	
			</div>
		</section>


		<section class="reviews">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="titulo">-What our clients say about us-</h3>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 cliente">
						<div class="foto">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/cliente.jpg" alt="">
						</div>
						<div class="review">
							<p class="texto">“Best school to learn Surfing. I truly recommend this surf school! I took an 8-lesson surf camp from there and I could really see the results of having basically no experience to catching waves by myself on hardboard and actually understanding what I'm doing!”</p>
							<p class="nombre"> -Elis L.</p>
						</div>
					</div>
					<div class="col-md-12 cliente derecha">
						<div class="foto">
							<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/cliente1.jpg" alt="">
						</div>
						<div class="review">
							<p class="texto">“It´s a real paradise for Surfers. I had an amazing time with these kind people, want to thank my coach Sahan & Coffee for the memorable time I spend in Welligama. It was so cool to be one of the guys, love & peace. I really miss the good vibes ”</p>
							<p class="nombre">- laantje4</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>


		</section>

	<section class="bring">
		<div class="contenedor">
				<div class="articulo"><h2 class="br">-What to bring?-</h2>
				<p class="textlesson"><i class="fa fa-sun-o"></i> Coming to the tropical coast is always super fun. <br><i class="fa fa-check-circle"></i>	All you need to bring is a swimsuit, towel, sunscreen and some water. We will provide you with a rash shirt during your lesson. <br> <i class="fa fa-map-signs"></i>	We do have a changing room, and there are tons of restaurants and cafes next to our school, if you feel hungry or thirsty.
				</p>
			</div>
				
		</div>
	</section>

<?php get_footer(); ?>


