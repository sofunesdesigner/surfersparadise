// efectos menu//

$(document).ready(function () {

	ww = $(window).width(); 
	if (ww > 800) { 
		$('.menu a').each(function (index, elemento) {
			$(this).css({
				'top': '-200px'
			});
			$(this).animate({top: '0'}, 2000 + (index*500))
		});
	};

	//scroll elementos menu//

	var whatwedo = $("#whatwedo").offset().top;
	$('#btn-whatwedo').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: 700
		},500)
	});

	var whoweare = $('#whoweare').offset().top;
	$('#btn-whoweare').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: 2950
		},500)
	});

	var ourlessons = $('#ourlessons').offset().top;
	$('#btn-lessons').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: 4150
		},500)
	});

	var contact = $('#contact').offset().top;
	$('#btn-contact').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop: 6620
		},500)
	});
});